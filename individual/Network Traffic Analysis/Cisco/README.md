# Cisco (Easy)

 Analyze a Cisco router broadcasting via the Cisco Discovery Protocol and identify information about the router.



All of this information can be found by just opening the pcap file with wireshark.

1. What is the Device ID of the router?

`APb838.61f3.05ac`

2. On what day was the router's software last updated?

Check the information about the Software version: Compiled Tue 30-Jul-13 22:57 by prod_rel_team

` 2013-07-30 `

3. What is the product number of the Cisco router?

`AIR-CAP2602I-Q-K9`

4. What is the IP address of the Cisco router?

`192.168.10.10`