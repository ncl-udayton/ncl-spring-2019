# GTP (Medium)

 Analyze GTP traffic to understand how a mobile device connects to a GSM network.



1. What is the IP address of the mobile handset used in the UDP-transport layer?

`127.0.0.2`

2. What is the IP address of the SGSN used in the UDP-transport layer?

`127.0.0.1`

3. What country is the mobile handset located in?

Search for the `String` "country" in the `Packet details`

`Sweden`

4. What is the name of the mobile network of the mobile handset?

`Telia Sverige AB`

5. What is the Mobile Subscription Identification Number of the mobile handset?

???

6. What is the local phone number (without country code or dashes) of the mobile handset?

`46702123456`

7. What is the id used by the mobile handset to authenticate with the network?

`mig`

8. What is the password used by the mobile handset to authenticate with the network?

`hemmelig`