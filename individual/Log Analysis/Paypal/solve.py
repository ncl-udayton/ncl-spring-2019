from collections import defaultdict
from datetime import datetime
from pprint import pprint
import json
import re

merchantre = re.compile(r'{"merchant_id":"(.*)","email"')
shippingre = re.compile(r'"shipping":"(.*)"}},"payee"')


merchants = set()
shippings = set()
carts = set()
totals = defaultdict(float)
emails = set()

with open('Paypal.log') as f:
	for line in f:
		line = line[:-1]

		match = merchantre.search(line)
		if match: merchants.add(match.group(1))

		match = shippingre.search(line)
		if match: shippings.add(match.group(1))

		if 'DEBUG: Response Data:' in line:
			linejson = json.loads(line.split('DEBUG: Response Data: ')[1])
			#pprint(line, indent=1)

			if linejson.get('transactions') and linejson.get('state') == 'approved':
				for t in linejson.get('transactions'):
					totals[linejson['cart']] = float(t['amount']['total'])
					#pprint(t['amount']['total'], indent=1)
			if linejson.get('cart'):
				carts.add(linejson.get('cart'))

			if linejson.get('payer'):
				emails.add(linejson.get('payer').get('payer_info').get('email'))


print("merchants: %s" % ' '.join([a for a in merchants]))
print("shippings: %s" % ' '.join([a for a in shippings]))

first = datetime.strptime("04-15-2018 09:32:16", '%m-%d-%Y %H:%M:%S')
last  = datetime.strptime("05-01-2018 19:15:32", '%m-%d-%Y %H:%M:%S')
print("seconds between first and last: %d" % (last - first).total_seconds())


print("total number of payments processed %d" % len(carts))

print("total revenue: %.02f" % sum(totals.values()))
# total transactions: 52983.60000

pprint(emails, indent=1)
pprint(domains, indent=1)

# its not the user agent
