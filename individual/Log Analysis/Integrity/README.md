# Integrity (Medium)

 We believe a file on a server have been compromised. Use a file integrity check log to identify the corrupted file. All of the files have been moved to the /root/samples folder.



1. How many files are in the /root/samples folder?

```
root@integrity_medium:~$ ls -1 /root/samples/ | wc -l
1000
```

2. What is the file that does not match its corresponding hash in the integrity_check.log?

First recalculate the md5sums of every file. The reason I use a small bash script with a for loop is so I can take advantage of the `-v` flag for ls which will sort the files numerically.

```
root@integrity_medium:~$ for f in $(ls -1v ./samples/*); do md5sum $f; done > integrity_new.log
```

Then check the diff

```
root@integrity_medium:~$ diff integrity_check.log integrity_new.log 
673c673
< 3118cfdb2d4c8a1c2363b59e34a85816  ./samples/672.bin
---
> fce5e6ad5da2c2837d783e4952c49bfe  ./samples/672.bin
```

