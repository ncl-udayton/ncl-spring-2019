# Hidden Treasure (Medium)

 Test your scanning abilities. Find the hidden treasure on this HTTP [website](http://treasure4736850.cityinthe.cloud/).

First thing to check for is [robots.txt](https://treasure4736850.cityinthe.cloud/robots.txt) : `Allow: /honk`



This is part of the scanning challenge so run [dirbuster](https://www.owasp.org/index.php/Category:OWASP_DirBuster_Project) with the medium wordlist

[Sample scan settings](https://i.imgur.com/bDplqMk.png)

[Sample results](https://i.imgur.com/1UWTbDJ.png)



1. What is the first flag?

[SKY-UHOL-6214](https://treasure4736850.cityinthe.cloud/halloween/flag1.txt)

2. What is the second flag?

[SKY-ELJF-5446](https://treasure4736850.cityinthe.cloud/ghostbusters/flag2.txt)

3. What is the third flag?

[SKY-PZMA-5492](https://treasure4736850.cityinthe.cloud/bones/flag3.txt)

4. What is the fourth flag?

[SKY-VCSJ-7400](https://treasure4736850.cityinthe.cloud/frankenstein/flag4.txt)

5. What is the fifth flag?

[SKY-HONK-4216](https://treasure4736850.cityinthe.cloud/honk/flag5.txt)