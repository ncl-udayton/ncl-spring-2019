# CryptoKait (Hard)

 NCL Chief Player Ambassador, CryptoKait, found some flags while she was on her trip to Orlando. Find some of the flags she shared on Twitter and answer questions about her trip. Kait would love you to rate the challenge - just click on the feedback icon on the left sidebar.



1. What drink did Kait have at Galaxy's Edge (include the color)?

[blue milk](https://pbs.twimg.com/media/EF-EHn5X0AEWn28.jpg)

2. What was the name of the Droid best friend that Kait met at Disney?

`R3N1`

3. What did Kait name the droid she built at Disney World?

4. What scheme was used to encode a message that Kaitlyn found at Galaxy's Edge?

`Aurebesh`

5. What is the plaintext of the encoded message that Kait found at Galaxy's Edge?

`danger keep clear`

6. What flag did Kait share after she was chosen at the wand ceremony?

[NCL-WAND-8943](https://twitter.com/CryptoKait/status/1180190617147707399)

7. What flag did Kait share just before she took her flight back from Orlando?

[NCL-BUZZ-4816](https://pbs.twimg.com/media/EGL4N__WwAAQ-Xu.jpg:large)