# Capture the Flag (Medium)

 We have found an image of a hacker that has captured the flag! Help us retrieve the flag!



Use an [online tool](http://metapicz.com/) to look at image metadata.



1. What city or town is the hacker located in?

`Macedon`

2. On what date was this photo taken?

`2019-10-14`

3. What is the name of the business that owns the field in the image?

`Long Acre Farms`

4. What is the value of the flag?

`SKY-MAZE-1014`