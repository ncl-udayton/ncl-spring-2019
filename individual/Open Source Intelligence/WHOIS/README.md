# WHOIS (Medium)

 Research conduct open source intelligence data collection about cyberskyline.com. Answer the following questions as they relate to the cyberskyline.com domain.



Use an online search site like  https://whois.net/ 



1. Who is the registrar of this domain?

`Namecheap`

2. What is the IANA ID of the registrar?

`1068`

3. On what date was this domain first registered?

`Creation Date: 2014-04-15T19:03:26z`

4. What is this domain's registry domain id?

`1854866838_DOMAIN_COM-VRSN`