# Hashing (Easy)

These problems can be solved using built-in Linux command line tools.



1. What is the md5 hash of the password: "01GracefuL9o2842"

```
$ echo -n 01GracefuL9o2842 | md5sum
bd7220f733759cd5a2e69ca25ee8be0c *-
```

2. What is the sha1 hash of the password: "1spacestar2833"

```
$ echo -n 1spacestar2833 | sha1sum
bb397bc0b74670552a10a340e9184279ab38971e *-
```

3. What is the sha256 hash of the password: "1gooddog14202"

```
$ echo -n 1gooddog14202 | sha256sum
c777618af07788160dd2826d74a87b7759acdd19da60759559677e6bf90b4c3c *-
```

