# Cracking 2 (Easy)

 Our officers have obtained password dumps storing hacker passwords. Try your hand at cracking them.



These hashes appear to be [windows NTLM hashes]( https://medium.com/@petergombos/lm-ntlm-net-ntlmv2-oh-my-a9b235c58ed4 ). Make a text file with the hashes in the correct format. We will use [ophcrack]( https://ophcrack.sourceforge.io/download.php?type=ophcrack) to crack these hashes. They will typically use passwords from the [ophcrack XP rainbow tables]( https://ophcrack.sourceforge.io/tables.php). Start with the small wordlist and use the larger ones as needed.

Import the hashes by clicking `Load`-->`PWDUMP file`-->`hashes.txt`

```
grimreaper09
darkskulls
1badwitch
```

